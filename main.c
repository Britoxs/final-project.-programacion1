#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>

#define limpiar system("cls");
#define color_sistema color(Lblanco,negro);
#define espacio printf("\n");
#define dinero color(Lverde,negro);
#define espacio_margen ir (4,m++);

//*************************************************************  Variables globales  **********************************************************************************

int aleatorio, apuesta_total,num_apuesta_J1[20], valor_apuestaJ1[20],num_apuesta_J2[20], valor_apuestaJ2[20];
//Numero de apuestas define por lo que se ha apostado
//valor de apuesta define la cantidad a ganar-perder.
int creditos[2]={500,500};


//*************************************************************  Colors  ************************************************************************************************
                    //Color, Fondo
void color(int c, int b)
{
  HANDLE  hConsole;
  hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
  FlushConsoleInputBuffer(hConsole);
  SetConsoleTextAttribute(hConsole, c+16*b);
}
// ejemplo: color (blanco, verde);
enum {negro=0,azul, verde, aqua, rojo, morado, amarillo, blanco,
gris, azulc,Lverde,Laqua,Lrojo,Lmorado,Lamarillo,Lblanco};



void ir(int x,int y)
{
  COORD coord;
  coord.X = x;
  coord.Y = y;
  SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

void margen ()
{
   int x;

   dinero

   for (x=1;x<=60;x++)
   {
       ir (x,1); printf("%c",176);
       ir (x,30); printf("%c",176 );

        if (x<=30)
        {
            ir (1,x); printf("%c",176);
            ir (60,x); printf("%c",176);
        }
   }
   color_sistema
}

void instrucciones()
{

    int m=4;
    int yo;

    limpiar
    margen();

    espacio_margen
    color(Lamarillo,negro);
    printf("\t \t ------ Instrucciones------ ");
    color_sistema

    m++;
    ir (4,m++);
    printf("Para jugar, se recomienda tener una imagen"); espacio_margen printf("de la ruleta de casino para una mejor experiencia.");

    m++;

    espacio_margen

    printf("1. Leer las diferentes apuestas que puedes realizar"); espacio_margen printf  ("en el menu de seleccion.");

    m++;

   espacio_margen
    printf("2. Ingresar el numero al que quieres apostar y"); espacio_margen printf("selecciona el color de la ficha que prefieras.");

    m++;

    espacio_margen
    printf ("3. Ingresa la cantidad de fichas a apostar.");
    espacio_margen
    printf("En base a la ficha apostada seran tus ganacias.");

    m++;
    m++;
    espacio_margen
    printf("Invita a un amigo y prueba tu suerte.");

    m++;
    espacio_margen
    printf("\t \t-------%cDiviertete!-------",173);

    m++;
    espacio_margen
    printf("Pulsa 0 para regresar al menu.");
    espacio_margen
    printf("pulsa 1 para ver el menu de las ganancias.");

    espacio_margen
    scanf("%d",&yo);
    switch(yo)
    {
    case 0:
        {
            portada();
            break;
        }

    case 1:
        {
            ganancias();
            break;
        }
    default:
        {
        limpiar
        instrucciones();
        break;
        }
    }
}

void portada()
{
    int m=4, xd;

    limpiar
    margen();

    ir (19,4);
    color(amarillo,negro);
    printf("----- %cBienvenido! -----",173);
    color_sistema

    m++;
    m++;

    espacio_margen
    printf("\t   Aqui la simulacion de la ruleta de casino.");

    m++;
    m++;

    espacio_margen

    printf("\t %c-%c Ingresa 0 si quieres jugar. ",175,175);
     m++;
     espacio_margen
      printf("\t %c-%c Ingresa 1 si quieres ver ",175,175); espacio_margen printf("\t     el menu de instrucciones.");

     m++;
     m++;
     espacio_margen
     printf("\t %c-%c Ingresa 2 si quieres ver ",175,175); espacio_margen printf("\t     las ganancias de las apuestas.");

     m++;
     m++;
     espacio_margen
     color (aqua,negro);
     printf("\t \t          %c-%c ",175,175); printf("  %c-%c ",174,174);


     ir (30,19);
     scanf("%d",&xd);

     color_sistema
     espacio_margen
     m++;

     switch(xd)
     {
     case 0:
         {
            limpiar
             break;
         }
     case 1:
        {
          instrucciones();
            break;
        }
     case 2:
        {
            ganancias();
            break;
        }

     default:
        {
            limpiar
            portada();
        }
     }
}

void ganancias ()
{
    int m=4, xp;

    limpiar
    margen();

    ir (4,m);
    printf("\t Aqui se muestran las ganancias que tiene"); m++; espacio_margen printf("\t cada opcion a apostar");

    m++;
    m++;
    espacio_margen
    printf("1. Si apuestas a la opcion  de numero par o impar:"); espacio_margen printf("\t Al ganar tus apuestas se duplican");

    m++;
    espacio_margen
    printf("2. Si apuestas a la opcion de casilla roja o negra:"); espacio_margen printf("\t Al ganar tus apuestas se duplican");

    m++;
    espacio_margen
    printf("3. Si apuestas a la opcion de numeros en intervalos:"); espacio_margen printf("\t Al ganar tus apuestas se triplican");

    m++;
    espacio_margen
    printf("4. Si apuestas a la opcion una columna:"); espacio_margen printf("\t Al ganar tus apuestas se triplican");

    m++;
    espacio_margen
    printf("4. Si apuestas a la opcion de un numero especifico o 0"); espacio_margen printf("\t Al ganar tus apuestas se multiplican por 35");

    m++;

    espacio_margen
    printf("Estas ganancias se calculan en base con "); espacio_margen printf("el numero total de opciones posibles de ganar. ");

    m++;
    m++;

    espacio_margen

    printf("\t Ingresa 0 para regresar al menu. ");

     scanf("%d",&xp);

     if (xp==0) portada();
     else
        ganancias();
}


//**********************************************************************************************************************************************************************

int numero_aleatorio ()
{
  srand(time(NULL));
  aleatorio=rand()%37;

 return aleatorio;
}

//**********************************************************************************************************************************************************************

void menu_fichas()
{
    printf(" \n Estas son las diferentes fichas con \n su respectivo valor:  \n");
    color (Lmorado,negro);
    printf(" \n  \t 1. Rosa=50");
    color(rojo,negro);
    printf(" \n  \t 2. Roja=100");
    color (azul,negro);
    printf(" \n  \t 3. Azul=200");
    color (verde, negro);
    printf(" \n  \t 4. Verde=500 \n");
    color (blanco,negro);
}

//**********************************************************************************************************************************************************************

int  apuesta_J1(int x) //X ser� el iterador para cambiar entre localidad del arreglo
{
    int fichas [4]={50,100,200,500};
    int num_fichas;
    int selec_ficha;

        printf(" \n Color de la ficha a elegir. \n Ingrese el numero que corresponda: ");
        scanf("%d",&selec_ficha); fflush(stdin);

        switch(selec_ficha)
        {
 //********************************************************************************************************************************
            case 1:
            {
            while (1)
            {
                color (Lmorado, negro);

                printf("  \n Maximo de fichas posibles:  %d  \n Numero de fichas a apostar:  ",creditos [0]/fichas[0]);
                printf(" \n Ingresa 0 para regresar   " );
                scanf("%d",&num_fichas); fflush(stdin);

                    if (num_fichas*fichas[0]<=creditos[0])
                    {
                        valor_apuestaJ1[x]=num_fichas*fichas[0];
                        creditos[0]-=valor_apuestaJ1[x];
                        break;
                    }
                else { color_sistema printf("  \n El total de fichas excede el total de creditos.\n \t  Intente de nuevo\n \n");}
            }
            break;
        }
  //********************************************************************************************************************************
 case 2:
             {
            while (1)
            {
                color(rojo, negro);

                printf("  \n Maximo de fichas posibles:  %d \n Numero de fichas a apostar:  ",creditos [0]/fichas[1]);
                printf(" \n Ingresa 0 para regresar   " );
                scanf("%d",&num_fichas); fflush(stdin);

                    if (num_fichas*fichas[1]<=creditos[0])
                    {
                        valor_apuestaJ1[x]=num_fichas*fichas[1];
                        creditos[0]-=valor_apuestaJ1[x];
                        break;
                    }
                else { color_sistema printf("\n El total de fichas excede el total de creditos.\n \t Intente de nuevo\n \n");}
            }
            break;
        }
 //********************************************************************************************************************************
 case 3:
        {
            while (1)
            {
                color (azul, negro);

                printf(" \n Maximo de fichas posibles:  %d \n Numero de fichas a apostar.  ",creditos [0]/fichas[2]);
                printf(" \n Ingresa 0 para regresar   " );
                scanf("%d",&num_fichas); fflush(stdin);

                    if (num_fichas*fichas[2]<=creditos[0])
                    {
                        valor_apuestaJ1[x]=num_fichas*fichas[2];
                        creditos[0]-=valor_apuestaJ1[x];
                        break;
                    }
                else { color_sistema printf("\n El total de fichas excede el total de creditos.\n \t Intente de nuevo\n \n");}
            }
            break;
        }

 case 4:
            {
            while (1)
            {
                color (verde,negro),

                printf(" \n Maximo de fichas posibles:  %d \n Numero de fichas a apostar:  ",creditos [0]/fichas[3]);
                printf(" \n Ingresa 0 para regresar   " );
                scanf("%d",&num_fichas); fflush(stdin);

                    if (num_fichas*fichas[3]<=creditos[0])
                    {
                        valor_apuestaJ1[x]=num_fichas*fichas[3];
                        creditos[0]-=valor_apuestaJ1[x];
                        break;
                    }
                else { color_sistema printf("\n El total de fichas excede el total de creditos.\n \t Intente de nuevo\n \n");}
            }
            break;
        }
 default:  printf("\n No es un valor de ficha correcto.");

 }
return valor_apuestaJ1[x];
}

//**********************************************************************************************************************************************************************

int  apuesta_J2(int x) //X ser� el iterador para cambiar entre localidad del arreglo
{
    int fichas [4]={50,100,200,500};
    int num_fichas;
    int selec_ficha;

        printf("\n Color de la ficha a elegir.\n Ingrese el numero que corresponda: ");
        scanf("%d",&selec_ficha); fflush(stdin);

        switch(selec_ficha)
        {
 //********************************************************************************************************************************
            case 1:
            {
            while (1)
            {
                color (Lmorado, negro);

                printf(" \n Maximo de fichas posibles:  %d \n Numero de fichas a apostar:  ",creditos [1]/fichas[0]);
                printf(" \n Ingresa 0 para regresar   " );
                scanf("%d",&num_fichas); fflush(stdin);

                    if (num_fichas*fichas[0]<=creditos[1])
                    {
                        valor_apuestaJ2[x]=num_fichas*fichas[0];
                        creditos[1]-=valor_apuestaJ2[x];
                        break;
                    }
                else { color_sistema printf("\n El total de fichas excede el total de creditos.\n \t Intente de nuevo\n \n");}
            }
            break;
        }
  //********************************************************************************************************************************
 case 2:
             {
            while (1)
            {
                color(rojo, negro);

                printf(" \n Maximo de fichas posibles:  %d \n Numero de fichas a apostar:  ",creditos [1]/fichas[1]);
                printf(" \n Ingresa 0 para regresar   " );
                scanf("%d",&num_fichas); fflush(stdin);

                    if (num_fichas*fichas[1]<=creditos[1])
                    {
                        valor_apuestaJ2[x]=num_fichas*fichas[1];
                        creditos[1]-=valor_apuestaJ2[x];
                        break;
                    }
                else { color_sistema printf("\n El total de fichas excede el total de creditos.\n \t Intente de nuevo\n \n");}
            }
            break;
        }
 //********************************************************************************************************************************
 case 3:
        {
            while (1)
            {
                color (azul, negro);

                printf(" \n Maximo de fichas posibles:  %d \n Numero de fichas a apostar:  ",creditos [1]/fichas[2]);
                printf(" \n Ingresa 0 para regresar   " );
                scanf("%d",&num_fichas); fflush(stdin);

                    if (num_fichas*fichas[2]<=creditos[1])
                    {
                        valor_apuestaJ2[x]=num_fichas*fichas[2];
                        creditos[1]-=valor_apuestaJ2[x];
                        break;
                    }
                else { color_sistema printf("\n El total de fichas excede el total de creditos.\n \t Intente de nuevo\n \n");}
            }
            break;
        }

 case 4:
            {
            while (1)
            {
                color (verde,negro),

                printf(" \n Maximo de fichas posibles:  %d \n Numero de fichas a apostar:  ",creditos [1]/fichas[3]);
                printf(" \n Ingresa 0 para regresar   " );
                scanf("%d",&num_fichas); fflush(stdin);

                    if (num_fichas*fichas[3]<=creditos[1])
                    {
                        valor_apuestaJ2[x]=num_fichas*fichas[3];
                        creditos[1]-=valor_apuestaJ2[x];
                        break;
                    }
                else { color_sistema printf("\n El total de fichas excede el total de creditos.\n \t Intente de nuevo\n \n");}
            }
            break;
        }
 default:  printf("\n No es un valor de ficha correcto.");

 }
return valor_apuestaJ2[x];
}

void menu (int x)
{
    color (aqua, negro);
    printf("\n \n Estas son tus opciones para apostar:\n");
    color (Lamarillo, negro);
    printf("\n \tCreditos restantes: %d \n",creditos[x]);

    color (Lblanco, negro);
    printf("\n  1. Apostar a las casillas rojas.");
    printf("\n  2. Apostar a las casillas negras. \n");
    printf("\n  3. Apostar a un numero par.");
    printf("\n  4. Apostar a un numero impar. \n");
    printf("\n  5. Apostar al 0. \n");
    printf("\n  6. Apostar un numero entre 1-12.");
    printf("\n  7. Apostar un numero entre 1-18. \n");
    printf("\n  8. Apostar un numero entre 13-24.");
    printf("\n  9. Apostar un numero entre 19-36. \n");
    printf("\n  10. Apostar un numero entre 25-36.");
    printf("\n\n  11. Apostar a la columna 1.");
    printf("\n  12. Apostar a la columna 2.");
    printf("\n  13. Apostar a la columna 3. \n");
    printf("\n  14. Apuesta a un numero especifico.\n");

}


//************************************************************** Funcion principal *********************************************************************************
    int main()
    {
     int comparar; //Variable para usar en los calculos como numero aleatorio
     int num_especifico_J1[15]; //variable de ayuda para la opci�n 14 del switch
    int num_especifico_J2[15];
    char continuar [2];

    //*********** Arreglos de ayuda para el proceso de datos- **************
    int rojas[]={1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36};
    int negras[]={2,4,6,8,10,11,13,15,17,20,22,24,26,28,29,31,33,35};
    int columna1[]={1,4,7,10,13,16,19,22,25,28,31,34};
    int columna2[]={2,5,8,11,14,17,20,23,26,29,32,35};
    int columna3[]={3,6,9,12,15,18,21,24,27,30,33,36};

    portada();

          Brito:
  //************************************************************** Pedir datos al usuario ****************************************************************************

            for (int o=0; o<2;o++) //2 por la cantidad de jugadores posibles.
            {
                char pregunta[2]; //array para preguntar al usuario.

                menu(o); // Muestra el menu de seleccion.

                    for (int i=0;i<=10;i++)
                    {
                        printf(" \n \n Ingresa el numero de la opcion a apostar ");
                        printf(" \n     Ingresa 0 si no deseas apostar. \n \t \t   ");

                        if (o==0)
                            {
                                scanf("%d", &num_apuesta_J1[i]); fflush(stdin);

                                if (num_apuesta_J1[i]==0)
                                break;

                                bucle1:
                                if (num_apuesta_J1[i]==14)
                                    {
                                        printf("\n Introduce el numero especifico al que apostar: ");
                                        scanf("%d",&num_especifico_J1[i]); fflush(stdin);

                                        if (num_especifico_J1[i]>37)
                                        {
                                            printf("\n No se permiten valores mayores a 37");
                                            goto bucle1;
                                        }
                                            if (num_especifico_J1[i]<0)
                                        {
                                            printf("\n No se permiten valores menores a 1");
                                            goto bucle1;
                                        }
                                    }

                                if (num_apuesta_J1[i]>15)
                                {
                                color (rojo,negro);
                                printf(" \n No es un valor de apuesta correcto. \n ");
                                goto Brito;
                                }
                            }

                        else

                        {
                         scanf("%d", &num_apuesta_J2[i]); fflush(stdin);
                        if (num_apuesta_J2[i]==0)
                        break;

                           bucle2:
                                if (num_apuesta_J2[i]==14)
                                    {
                                        printf("\n Introduce el numero especifico al que apostar: ");
                                        scanf("%d",&num_especifico_J2[i]); fflush(stdin);

                                        if (num_especifico_J2[i]>37)
                                        {
                                            printf("\n No se permiten valores mayores a 37");
                                            goto bucle2;
                                        }
                                            if (num_especifico_J2[i]<0)
                                        {
                                            printf("\n No se permiten valores menores a 1");
                                            goto bucle2;
                                        }
                                    }

                            if (num_apuesta_J2[i]>15)
                            {
                                color (rojo,negro);
                                printf(" \n No es un valor de apuesta correcto. \n ");
                                goto Brito;
                            }
                        }

                        menu_fichas();

                        if (o==0) apuesta_J1(i);
                        else apuesta_J2(i);

                        color (Lamarillo, negro);
                        printf(" \n \n Los creditos restantes son: %d", creditos[o]);

                        color_sistema

                        if (creditos[o]<1) break;
                    }

                if (o==0)
                    {
                        printf(" \n \n El jugador 2 %cdesea apostar?  ",168);
                        scanf("%s",&pregunta); fflush(stdin);
                        if (strcmp(pregunta,"no")==0) break;
                        else limpiar
                    }
                }

//******************************************************* Mostrar numero que ha salido *************************************************************
    limpiar
    numero_aleatorio(); //se genera el numero aleatorio.
    comparar=aleatorio; // el valor de aleatorio se guarda en otra variable.

    printf("\n \n El numero que ha salido en la ruleta es: ");
    espacio
    espacio
    printf("\t \t ");

    for (int m=0;m<=18;m++)
        {
            if (rojas[m]==aleatorio)
                {
                    color (rojo,blanco);
                    break;
                }
                else
                    color (negro,blanco);
            }
    printf("%d", aleatorio);
    color_sistema
    printf("\n \n \n");

//******************************************************* Procesamiento de los datos  obtenidos*************************************************************



                                        //********************* Jugador 1 **********************
    color(gris,negro);
    printf ("\n Ganancias del jugador 1: \n");
    color_sistema

    for (int i=0; i<20;i++)  //Tama�o de vector numero de apuestas
    {
        switch(num_apuesta_J1[i])
        {
            case 1:
            {
//************************** Fichas rojas o negras ***************************************
                for (int m=0;m<=18;m++)
                {
                    if (comparar==rojas[m])
                    {
                        valor_apuestaJ1[i]*=2;
                        creditos[0]+=valor_apuestaJ1[i];

                        espacio

                        color (rojo, blanco);
                        printf("La casilla era roja, %cGanaste!",173);
                        color_sistema

                        dinero
                        espacio

                        printf("\t Has ganado: %d",valor_apuestaJ1[i]);
                        color_sistema
                        espacio
                    }
                }
                 break;
            }

            case 2:
            {
                for (int m=0;m<=18;m++)
                {
                    if (comparar==negras[m])
                    {
                        valor_apuestaJ1[i]*=2;
                        creditos[0]+=valor_apuestaJ1[i];

                        espacio
                         color (negro,blanco);

                        printf(" La casilla era negra, %chas ganado!",173);

                        color_sistema
                        espacio
                        dinero

                        printf("\t Has ganado: %d",valor_apuestaJ1[i]);
                        color_sistema
                        espacio
                    }
                }
                break;
            }
//***************************** Par o impar ***************************************

        case 3:
        {
            if (comparar%2==0)
                {
                    valor_apuestaJ1[i]*=2;
                    creditos[0]+=valor_apuestaJ1[i];

                    espacio
                    color(azul,blanco);

                    printf("Enhorabuena, %cel numero era par!",173);

                    color_sistema
                    espacio
                    dinero

                    printf("\t Has ganado: %d",valor_apuestaJ1[i]);
                    color_sistema
                    espacio


                }
                    break;
        }

       case 4:
        {
            if (comparar%2!=0)
                {
                    valor_apuestaJ1[i]*=2;
                    creditos[0]+=valor_apuestaJ1[i];

                    espacio
                    color(Lmorado,blanco);

                     printf(" %cEl numero es impar!",173);

                     color_sistema
                     espacio
                     dinero

                     printf("\t Has ganado: %d",valor_apuestaJ1[i]);
                     color_sistema
                     espacio

                }
                    break;
        }

//********************* El numero es 0 ************************
        case 5:
        {
            if (comparar==0)
                {
                    valor_apuestaJ1[i]*=35;
                    creditos[0]+=valor_apuestaJ1[i];

                    espacio
                    color(Lverde, amarillo);

                    printf("%cHa salido 0!! Enhorabuena!",173);

                    color_sistema
                    espacio
                    dinero

                    printf("\t Has ganado: %d",valor_apuestaJ1[i]);
                    color_sistema
                    espacio

                }
                    break;
        }


 //***************** 1-12 o 1-18 ********************
        case 6:
        {
            if (0<comparar<=12)
                {
                    valor_apuestaJ1[i]*=3;
                    creditos[0]+=valor_apuestaJ1[i];

                    color(morado,negro);
                    printf("\n El numero esta entre 1 y 12");

                    dinero
                    espacio

                    printf("\n \t Has ganado: %d",valor_apuestaJ1[i]);
                    color_sistema
                    espacio
                }
                    break;
        }

        case 7:
        {
            if (comparar<=18)
            {
                    valor_apuestaJ1[i]*=3;
                    creditos[0]+=valor_apuestaJ1[i];

                    color (rojo,negro);
                    printf("\n El numero esta entre 1 y 18");

                    dinero
                    espacio

                    printf("\n \t Has ganado: %d",valor_apuestaJ1[i]);
                    color_sistema
                    espacio
            }
                break;
        }

 //***************** Entre 13 -24 o 19-26 ********************

        case 8:
        {
            if (12<comparar<25)
            {
                    valor_apuestaJ1[i]*=3;
                    creditos[0]+=valor_apuestaJ1[i];

                    color(aqua,negro);
                    printf("\n El numero esta entre 13 y 24");

                    dinero
                    espacio
                    printf("\n \t Has ganado: %d",valor_apuestaJ1[i]);

                    color_sistema
                    espacio
            }
                break;
        }

        case 9:
        {
            if (18<comparar<27)
            {
                    valor_apuestaJ1[i]*=3;
                    creditos[0]+=valor_apuestaJ1[i];

                    color(gris,negro);
                    printf("\n El numero esta entre 19 y 36");

                    dinero
                    espacio

                    printf("\n \t Has ganado: %d",valor_apuestaJ1[i]);
                    color_sistema
                    espacio
            }
                break;
        }

    //***************** Entre 25- 36 ********************

        case 10:
        {
            if (24<comparar<37)
            {
                    valor_apuestaJ1[i]*=3;
                    creditos[0]+=valor_apuestaJ1[i];

                    color (morado, negro);
                    printf("\n El numero esta entre 25 y 36");

                    dinero
                    espacio

                    printf("\n \t Has ganado: %d",valor_apuestaJ1[i]);
                    color_sistema
                    espacio
            }
                break;
        }

//***************** Columnas ********************

        case 11:
        {
                for (int m=0;m<12;m++)
                {
                    if (comparar==columna1[m])
                    {
                        valor_apuestaJ1[i]*=2;
                        creditos[0]+=valor_apuestaJ1[i];

                        color(Lmorado,negro);
                        printf("\n  El numero pertenece a la columna 1");

                        dinero
                        espacio
                        printf("\n \t Has ganado: %d",valor_apuestaJ1[i]);

                        color_sistema
                        espacio
                    }
                }
                break;
        }

        case 12:
        {
                for (int m=0;m<12;m++)
                {
                    if (comparar==columna2[m])
                    {
                        valor_apuestaJ1[i]*=2;
                        creditos[0]+=valor_apuestaJ1[i];

                        color (Lmorado,negro);
                        printf("\n  El numero pertenece a la columna 2");

                        dinero
                        espacio
                        printf("\n \t Has ganado: %d",valor_apuestaJ1[i]);

                        color_sistema
                        espacio
                    }
                }
                break;
        }

        case 13:
        {
                for (int m=0;m<12;m++)
                {
                    if (comparar==columna2[m])
                    {
                        valor_apuestaJ1[i]*=2;
                        creditos[0]+=valor_apuestaJ1[i];

                        color (Lmorado,negro);
                        printf("\n  El numero pertenece a la columna 3");

                        dinero
                        espacio

                        printf("\n \t Has ganado: %d",valor_apuestaJ1[i]);
                        color_sistema
                        espacio
                    }
                }
                break;
        }

        case 14:
            {
              for (int r=0;i<15;i++)
              {
                 if (comparar==num_especifico_J1[r])
                    {
                        valor_apuestaJ1[i]*=35;
                        creditos[0]+=valor_apuestaJ1[r];

                        espacio
                        color(rojo,blanco);

                        printf("%c%cEl numero %d ha salido en la ruleta!!",173,173,num_especifico_J1[r]);

                        dinero
                        espacio

                        printf("\n Has ganado: %d",valor_apuestaJ1[i]);
                        color_sistema
                        espacio
                    }
              }
            }

        default: break;
        }
        comparar=aleatorio;
    }

                                        //******************Jugador 2 *********************
    color(gris,negro);
    printf ("\n Ganancias del jugador 2: \n");
    color_sistema


        for (int i=0; i<20;i++)  //Tama�o de vector numero de apuestas
    {
        switch(num_apuesta_J2[i])
        {
            case 1:
            {
//************************** Fichas rojas o negras ***************************************
                for (int m=0;m<=18;m++)
                {
                    if (comparar==rojas[m])
                    {
                        valor_apuestaJ2[i]*=2;
                        creditos[1]+=valor_apuestaJ2[i];

                        espacio
                        color (rojo,blanco);

                        printf("%cLa casilla era roja, has ganado!",173);

                        dinero

                        printf("\n \t Has ganado: %d",valor_apuestaJ2[i]);

                        color_sistema
                        espacio
                    }
                }
                 break;
            }

            case 2:
            {
                for (int m=0;m<=18;m++)
                {
                    if (comparar==negras[m])
                    {
                        valor_apuestaJ2[i]*=2;
                        creditos[1]+=valor_apuestaJ2[i];

                        espacio
                        color(negro,blanco);
                        printf("%cLa casilla era negra, has ganado!",173);

                        dinero
                        printf("\n \t Has ganado: %d",valor_apuestaJ2[i]);

                        color_sistema
                        espacio
                    }
                }
                break;
            }
//***************************** Par o impar ***************************************

        case 3:
        {
            if (comparar%2==0)
                {
                    valor_apuestaJ2[i]*=2;
                    creditos[1]+=valor_apuestaJ2[i];

                    color(morado,negro);

                    printf("\n %cEnhorabuena, el numero era par!",173);

                    dinero
                    printf("\n \t Has ganado: %d",valor_apuestaJ2[i]);

                    color_sistema
                    espacio
                }
                    break;
        }

       case 4:
        {
            if (comparar%2!=0)
                {
                    valor_apuestaJ2[i]*=2;
                    creditos[1]+=valor_apuestaJ2[i];

                     color(morado,negro);

                     printf("\n %cEl numero es impar!",173);

                     dinero
                     printf("\n \t Has ganado: %d",valor_apuestaJ2[i]);

                     color_sistema
                     espacio
                }
                    break;
        }

//********************* El numero es 0 ************************
        case 5:
        {
            if (comparar==0)
                {
                    valor_apuestaJ2[i]*=35;
                    creditos[1]+=valor_apuestaJ2[i];

                    espacio
                    color(Lverde,blanco);

                    printf("%c%cHa salido 0!! Enhorabuena!",173,173);

                    dinero

                    printf("\n  \t Has ganado: %d",valor_apuestaJ2[i]);
                    color_sistema
                    espacio
                }
                    break;
        }


 //***************** 1-12 o 1-18 ********************
        case 6:
        {
            if (0<comparar<=12)
                {
                    valor_apuestaJ2[i]*=3;
                    creditos[1]+=valor_apuestaJ2[i];

                    color(azul,negro);

                    printf("\n El numero esta entre 1 y 12");

                    dinero

                    printf("\n \t Has ganado: %d",valor_apuestaJ2[i]);

                    color_sistema
                    espacio
                }
                    break;
        }

        case 7:
        {
            if (comparar<=18)
            {
                    valor_apuestaJ2[i]*=3;
                    creditos[1]+=valor_apuestaJ2[i];

                    color(azul,negro);

                    printf("\n El numero esta entre 1 y 18");

                    dinero
                    printf("\n Has ganado: %d",valor_apuestaJ2[i]);

                    color_sistema
                    espacio
            }
                break;
        }

 //***************** Entre 13 -24 o 19-26 ********************

        case 8:
        {
            if (12<comparar<25)
            {
                    valor_apuestaJ2[i]*=3;
                    creditos[1]+=valor_apuestaJ2[i];

                    color(azulc,negro);

                    printf("\n El numero esta entre 13 y 24");

                    dinero

                    printf("\n Has ganado: %d",valor_apuestaJ2[i]);

                    color_sistema
                    espacio
            }
                break;
        }

        case 9:
        {
            if (18<comparar<27)
            {
                    valor_apuestaJ2[i]*=3;
                    creditos[1]+=valor_apuestaJ2[i];

                    color(azulc,negro);

                    printf("\n El numero esta entre 19 y 36");

                    dinero

                    printf("\n Has ganado: %d",valor_apuestaJ2[i]);

                    color_sistema
                    espacio
            }
                break;
        }

    //***************** Entre 25- 36 ********************

        case 10:
        {
            if (24<comparar<37)
            {
                    valor_apuestaJ2[i]*=3;
                    creditos[1]+=valor_apuestaJ2[i];

                    color(morado,negro);

                    printf("\n El numero esta entre 25 y 36");

                    dinero

                    printf("\n Has ganado: %d",valor_apuestaJ2[i]);

                    color_sistema
                    espacio
            }
                break;
        }

//***************** Columnas ********************

        case 11:
        {
                for (int m=0;m<12;m++)
                {
                    if (comparar==columna1[m])
                    {
                        valor_apuestaJ2[i]*=2;
                        creditos[1]+=valor_apuestaJ2[i];

                        color (morado,negro);

                        printf("\n El numero pertenece a la columna 1");

                        dinero

                        printf("\n Has ganado: %d",valor_apuestaJ2[i]);

                        color_sistema
                        espacio
                    }
                }
                break;
        }

        case 12:
        {
                for (int m=0;m<12;m++)
                {
                    if (comparar==columna2[m])
                    {
                        valor_apuestaJ2[i]*=2;
                        creditos[1]+=valor_apuestaJ2[i];

                        color (morado,negro);

                        printf("\n  El numero pertenece a la columna 2");

                        dinero

                        printf("\n Has ganado: %d",valor_apuestaJ2[i]);

                        color_sistema
                        espacio
                    }
                }
                break;
        }

        case 13:
        {
                for (int m=0;m<12;m++)
                {
                    if (comparar==columna2[m])
                    {
                        valor_apuestaJ2[i]*=2;
                        creditos[1]+=valor_apuestaJ2[i];

                        color(morado,negro);

                        printf("\n El numero pertenece a la columna 3");

                        dinero

                        printf("\n Has ganado: %d",valor_apuestaJ2[i]);

                        color_sistema
                        espacio
                    }
                }
                break;
        }

        case 14:
            {
              for (int r=0;i<15;i++)
              {
                 if (comparar==num_especifico_J2[r])
                    {
                        valor_apuestaJ2[i]*=35;
                        creditos[1]+=valor_apuestaJ2[r];

                        color(Lverde,negro);

                        printf("\n  %c%cEl numero %d ha salido en la ruleta!!",173,173,num_especifico_J2[r]);

                        dinero

                        printf("\n Has ganado: %d",valor_apuestaJ2[i]);

                        color_sistema
                        espacio
                    }
              }
            }

        default: break;
        }
        comparar=aleatorio;
    }

        color(Lamarillo,negro);
        printf("\n Los creditos del jugador 1 son: %d \n\n ", creditos[0]);
        printf("\n Los creditos del jugador 2 son: %d \n\n ", creditos[1]);

        color_sistema
        printf("\n \n \n %cDesea seguir jugando? ",168);
        scanf("%s",&continuar); fflush(stdin);

        if (strcmp(continuar, "si")==0)
        {
            limpiar
            goto Brito;
        }

        else printf("\n \n  Gracias por jugar c:");

    color (negro, negro);
    return 0;
    }
